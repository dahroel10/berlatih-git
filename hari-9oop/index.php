<?php

require("animal.php");
require("ape.php");
require("frog.php");

$object = new animal("Shaun");

echo "Nama Hewan : $object->name <br>";
echo "Jumlah Kaki : $object->legs <br>";
echo "Berdarah Dingin :  $object->cold_blooded <br><br>";

$object2 = new Ape("Kera Sakti");
echo "Nama Hewan : $object2->name <br>";
echo "Jumlah Kaki : $object2->legs <br>";
echo "Berdarah Dingin : $object2->cold_blooded <br>";
echo "Kera Bersuara : ";
$object2->yell();

$object3 = new Frog("Buduk");
echo "<br> <br> Nama Hewan : $object3->name <br>";
echo "Jumlah Kaki : $object3->legs <br>";
echo "Berdarah Dingin : $object3->cold_blooded <br>";
echo "Kodok Melompat : ";
$object3->jump();



?>