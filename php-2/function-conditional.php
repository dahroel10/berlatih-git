<!DOCTYPE html>
<html lang="en">

<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta http-equiv="X-UA-Compatible" content="ie=edge">
<title>Function</title>
</head>

<body>
<h1>Berlatih Function PHP</h1>
<?php

echo "<h3> Soal No 1 Greetings </h3>";
function greetings($string){
    echo "Halo $string, Selamat Datang di Jabar Coding Camp <br>";
}
greetings("Bagas");
greetings("Wahyu");
greetings("Abdul");
echo "<br>";


echo "<h3>Soal No 2 Reverse String</h3>";
function reverse($kata){
    $lenght = strlen($kata);
    $tampung = " ";
    for ($i=($lenght-1); $i >= 0 ; $i--) { 
        $tampung .= $kata[$i];
    }
    return $tampung;
}
function reverseString($kata2){
    $balik=reverse($kata2);
    echo $balik . "<br>";
}

reverseString("abduh");
reverseString("Jabar Coding Camp");
reverseString("We Are JCC Developers");
echo "<br>";

echo "<h3>Soal No 3 Palindrome </h3>";
// Code function di sini
function pal($word){
    $word_len = strlen($word) - 1;
    $output = '';

    for ($x = $word_len; $x >= 0 ; $x--) { 
        $output .= $word[$x];
    }

    if($output == $word){
        echo "Palindrome ($word) => $output : " ."True ";
    }else {
        echo "Palindrome ($word) => $output : " ."False ";
    }
}
function palindrome($word2){
    $bolak=pal($word2);
    echo $bolak . "<br>";
}
palindrome("civic") ; // true
palindrome("nababan") ; // true
palindrome("jambaban"); // false
palindrome("racecar"); // true

echo "<h3>Soal No 4 Tentukan Nilai </h3>";
function tentukan_nilai($int){
    $tampung = "";
    if($int >= 85){
        $tampung.= "Sangat Baik";
    }elseif($int >= 70 && $int < 85){
        $tampung.= "Baik";
    }elseif($int >= 60 && $int <70){
        $tampung.= "Cukup";
    }else{
        $tampung.= "Kurang";
    }
    return $tampung . "<br>";
}
echo tentukan_nilai(98); //Sangat Baik
echo tentukan_nilai(76); //Baik
echo tentukan_nilai(67); //Cukup
echo tentukan_nilai(43); //Kurang


?>

</body>

</html>