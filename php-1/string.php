<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>String PHP</title>
</head>
<body>
    <h1>Berlatih String PHP</h1>
    <?php   
        echo "<h3> Soal No 1</h3>";
        $first_sentence = "Hello PHP!" ;
        $panjangkalimat = strlen($first_sentence);
        $jumlahkata = str_word_count($first_sentence);
        echo "Kalimat : $first_sentence <br>";
        echo "Panjang String : $panjangkalimat <br>";
        echo "Jumlah Kata : $jumlahkata <br>";
        
        $second_sentence = "I'm ready for the challenges";
        $panjangkalimat = strlen($second_sentence);
        $jumlahkata = str_word_count($second_sentence);
        echo "<br> Kalimat : $second_sentence <br>";
        echo "Panjang String : $panjangkalimat <br>";
        echo "Jumlah Kata : $jumlahkata <br>";

        echo "<h3> Soal No 2</h3>";
        /* 
            SOAL NO 2
            Mengambil kata pada string dan karakter-karakter yang ada di dalamnya. 
            
            
        */
        $string2 = "I love PHP";
        
        echo "<label>String: </label> \"$string2\" <br>";   
        echo "Kata pertama: " . substr($string2, 0, 1) . "<br>" ;
        echo "Kata kedua: " . substr($string2, 2,5) . "<br>" ;
        echo "Kata Ketiga: " .  substr($string2, 6,8) . "<br>";

        echo "<h3> Soal No 3 </h3>";
        $string3 = "PHP is old but Good!";
        echo "String: \"$string3\" "; 
        echo "<br> Kalimat String Yang Diganti : " . str_replace("Good!", "awesome!", $string3)
        // OUTPUT : "PHP is old but awesome"

    ?>
</body>
</html>